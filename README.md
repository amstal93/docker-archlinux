# Archlinux docker image

> This is an up-to-date fork from [Pierre Penninckx's original repository](https://github.com/ibizaman/docker-archlinux).

Official `archlinux:latest` docker image with AUR support.

Since AUR packages cannot be built as root, this image uses the unprivileged user `nobody` to install them:

```docker
RUN aur-install.sh -S arduino
```

You can also pass a second argument for, say, skip checksums:

```docker
RUN aur-install.sh -S arduino --skipchecksums
```

Unfortunately, this script does not install the package's dependencies so they need to be installed explicitly beforehand.

## Releases

### `latest`

Rebuilt automatically every Monday. Additionally, after each commit (and merge) to master.

### `release-{version}-{year}-{week}`

Snapshot of the `latest` tag after the automated weekly build on Mondays.
