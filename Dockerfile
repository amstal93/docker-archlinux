FROM archlinux:latest

RUN pacman --quiet --noconfirm -Syy && \
    pacman --quiet --noconfirm -Syu archlinux-keyring base-devel git

RUN if [ ! -z "$(pacman -Qtdq)" ]; then \
        pacman --noconfirm -Rns $(pacman -Qtdq); \
        fi && \
    pacman -Scc --noconfirm

COPY aur-install.sh /usr/bin/
RUN chmod a+x /usr/bin/aur-install.sh && \
    usermod --home /tmp/nobody --shell /bin/sh --expiredate= nobody

CMD /bin/sh
